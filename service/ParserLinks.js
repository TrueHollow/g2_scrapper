const cheerio = require('cheerio');
const Net = require('./Net');
const IO = require('./IO');
const config = require('../config');
const ParserReviews = require('./ParserReviews');
const logger = require('../logger')('service/ParserLinks.js');

const URL_TEMPLATE = 'https://www.g2.com';
const REGEX = /next/i;

class ParserLinks {
  static GetNextLink($) {
    let url = null;
    $('.pagination__named-link').each((i, tag) => {
      const str = $(tag).text();
      if (REGEX.test(str)) {
        const { href } = tag.attribs;
        if (href) {
          url = href;
        } else {
          logger.warn('Strange link (%s)', JSON.stringify(tag));
        }
      }
    });
    return url;
  }

  static GetLinksToReviews($) {
    const urlArr = [];
    $('.product-listing__product-name').each((i, tag) => {
      const { parent } = tag;
      if (parent && parent.type === 'tag' && parent.name === 'a') {
        const { href } = parent.attribs;
        if (href) {
          urlArr.push(href);
        } else {
          logger.warn('Strange link (%s)', JSON.stringify(parent));
        }
      } else {
        logger.warn('Strange markup');
      }
    });
    return urlArr;
  }

  static async ProcessReviews(reviewsUrl, c) {
    const copy = reviewsUrl.slice();
    const { industry, category } = c;
    const { RequestsLimit } = config.Parser;
    do {
      const iteration = copy.splice(0, RequestsLimit);
      // eslint-disable-next-line no-await-in-loop
      const reviews = await Promise.all(
        iteration.map(url => ParserReviews.getReview(url))
      );
      const finalData = [].concat(...reviews).map(r => {
        const review = r;
        review.industry = industry;
        review.category = category;
        return review;
      });
      // eslint-disable-next-line no-await-in-loop
      await IO.SaveResult(finalData);
    } while (copy.length);
  }

  static async ParseGategory(category) {
    const { url } = category;
    let fullUrl = `${URL_TEMPLATE}${url}`;
    logger.info('Parsing %s', fullUrl);
    do {
      // eslint-disable-next-line no-await-in-loop
      const html = await Net.GetHtml(fullUrl);
      const $ = cheerio.load(html);
      fullUrl = ParserLinks.GetNextLink($);
      const reviewsUrl = ParserLinks.GetLinksToReviews($);
      // eslint-disable-next-line no-await-in-loop
      await ParserLinks.ProcessReviews(reviewsUrl, category);
    } while (fullUrl);
  }
}

module.exports = ParserLinks;

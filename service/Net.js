const request = require('request');
const UserAgent = require('user-agents');
const logger = require('../logger')('service/Net.js');
const config = require('../config');

let position = -1;
const getNewProxy = () => {
  const { ProxyPool } = config;
  if (!Array.isArray(ProxyPool) || ProxyPool.length === 0) {
    return;
  }
  position += 1;
  if (position >= ProxyPool.length) {
    position = 0;
  }
  const proxy = ProxyPool[position];
  process.env.http_proxy = proxy;
  process.env.https_proxy = proxy;
};

const userAgentOptions = { deviceCategory: 'desktop' };

const HTTP_OK = 200;

const delay = async (timeout = 5000) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

const GetRequest = async url => {
  return new Promise((resolve, reject) => {
    getNewProxy();
    request(
      {
        url,
        method: 'GET',
        headers: {
          'User-Agent': new UserAgent(userAgentOptions).toString(),
        },
        gzip: true,
        timeout: 20000,
      },
      (err, incomingMessage, html) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(
              `Response code: ${incomingMessage.statusCode}, (${JSON.stringify(
                incomingMessage.headers
              )})`
            )
          );
        }
        return resolve(html);
      }
    );
  }).then(async data => {
    await delay(500);
    return data;
  });
};

const PostFormRequest = async (url, data) => {
  return new Promise((resolve, reject) => {
    getNewProxy();
    request(
      {
        url,
        method: 'POST',
        headers: {
          'User-Agent': new UserAgent(userAgentOptions).toString(),
        },
        gzip: true,
        json: true,
        timeout: 20000,
        form: data,
      },
      async (err, incomingMessage, json) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(
              `Response code: ${incomingMessage.statusCode}, (${JSON.stringify(
                incomingMessage.headers
              )})`
            )
          );
        }
        return resolve(json);
      }
    );
  }).then(async rData => {
    await delay(500);
    return rData;
  });
};

class Net {
  static async GetHtml(url) {
    let html;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        html = await GetRequest(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!html);
    return html;
  }

  static async PostJsonAsForm(url, data) {
    let json;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        json = await PostFormRequest(url, data);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!json);
    return json;
  }
}

module.exports = Net;

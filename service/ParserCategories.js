const cheerio = require('cheerio');
const Net = require('./Net');
const logger = require('../logger')('service/ParserCategories.js');

const URL = 'https://www.g2.com/categories';
const REGEX = /\/categories\//;

const getParentLinks = (tag, $) => {
  let parent = tag.parent();
  const result = [];
  let count = 0;
  do {
    parent = parent.parent();
    if (parent.hasClass('ml-2')) {
      count += 1;
      if (count > 1) {
        const a = $(parent.find('a').get(0));
        result.push(a.text().trim());
      }
    }
  } while (!parent.hasClass('newspaper-columns__list-item'));
  return result;
};

class ParserCategories {
  static async GetAll() {
    logger.info('Get all categories');
    const html = await Net.GetHtml(URL);
    const $ = cheerio.load(html);
    const urlArr = [];
    $('.paper .newspaper-columns__list-item').each((i, tagBlock) => {
      const categoryRoot = $('h2', tagBlock).get(0);
      if (!categoryRoot) {
        logger.warn('Unexpected category root.');
        return;
      }
      const CategoryRootText = $(categoryRoot)
        .text()
        .trim();
      const links = $(tagBlock).find('a');
      links.each((j, aTag) => {
        const { href } = aTag.attribs;
        if (REGEX.test(href)) {
          const a = $(aTag);
          const lastCategory = a.text().trim();
          const parents = getParentLinks(a, $);
          const industry = [CategoryRootText, ...parents, lastCategory].join(
            ' > '
          );
          urlArr.push({
            industryRootName: CategoryRootText,
            category: lastCategory,
            industry,
            url: href,
          });
        } else {
          logger.warn('Unexpected link (%s)', href);
        }
      });
    });
    return urlArr;
  }
}

module.exports = ParserCategories;

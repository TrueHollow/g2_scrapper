const cheerio = require('cheerio');
const Net = require('./Net');
const logger = require('../logger')('service/ParserReviews.js');

const URL_TEMPLATE = 'https://www.g2.com';

const REVIEWS_LIMIT = 25;
const REGEX_NEXT = /next/i;

const getNextLink = $ => {
  let result = null;
  $('.pagination__component a').each((i, tag) => {
    const text = $(tag).text();
    if (REGEX_NEXT.test(text)) {
      const { href } = tag.attribs;
      if (href) {
        result = href;
      }
    }
  });
  return result;
};

const improveKey = key => {
  switch (key) {
    case 'What do you like best?':
      return 'Likes';
    case 'What do you dislike?':
      return 'Dislikes';
    case 'Recommendations to others considering the product:':
      return 'Recommendations';
    case 'What problems are you solving with the product?  What benefits have you realized?':
      return 'Benefits';
    default:
      return key;
  }
};

class ParserReviews {
  static async getReview(url) {
    let fullUrl = `${URL_TEMPLATE}${url}`;
    const html = await Net.GetHtml(fullUrl);
    const result = [];
    let counter = 0;
    const $ = cheerio.load(html);

    const name = $('.product-head [itemprop="name"]')
      .children('a')
      .text()
      .trim();
    do {
      $('#reviews .paper').each((i, Tag) => {
        const title = $("[itemprop='name']", Tag)
          .text()
          .trim();
        const body = $(
          "div[itemprop='reviewBody'] div[itemprop='reviewBody']",
          Tag
        );
        let content = $(body)
          .text()
          .trim();
        if (title.length === 0 || content.length === 0) {
          logger.debug('Skipping video with title:', title);
        } else {
          const item = {
            name,
            title,
          };
          $('h5', body).each((k, hTag) => {
            const sel = $(hTag);
            const keyOriginal = sel.text().trim();
            content = content.replace(keyOriginal, '');
            const key = improveKey(keyOriginal);
            const value = sel
              .next()
              .text()
              .trim();
            content = content.replace(value, '');
            item[key] = value;
          });
          content = content.replace('Show More', '');
          item.content = content.trim();
          result.push(item);
          counter += 1;
        }
      });
      fullUrl = getNextLink($);
      if (!fullUrl) {
        break;
      }
    } while (counter < REVIEWS_LIMIT);

    return result;
  }
}

module.exports = ParserReviews;

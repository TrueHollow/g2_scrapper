const fs = require('fs');
const path = require('path');
const logger = require('../logger')('service/IO.js');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '../output');
const OUTPUT_FILENAME = 'reviews.json';

const createOutputDirectory = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(OUTPUT_DIRECTORY, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      logger.debug('Output directory was created.');
      return resolve();
    });
  });
};

let outputFile;
let lines = 0;

const createOutputFilestream = () => {
  const fullPath = path.resolve(OUTPUT_DIRECTORY, OUTPUT_FILENAME);
  outputFile = fs.createWriteStream(fullPath);
  return outputFile.write('[\n');
};

function isIterable(obj) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}

class IO {
  static async Init() {
    await createOutputDirectory();
    return createOutputFilestream();
  }

  static SaveResult(data) {
    if (!isIterable(data)) {
      throw new Error('Data must be iterable!');
    }
    logger.debug('Saving (%d) objects to output file.', data.length);
    // eslint-disable-next-line no-restricted-syntax
    for (const chunk of data) {
      if (lines) {
        outputFile.write(',\n');
      }
      outputFile.write(JSON.stringify(chunk, null, '\t'));
      lines += 1;
    }
  }

  static EndResult() {
    logger.info('Result has (%d) objects', lines);
    outputFile.end('\n]');
  }
}

module.exports = IO;

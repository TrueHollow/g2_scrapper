const logger = require('./logger')('index.js');
const IO = require('./service/IO');
const ParserCategories = require('./service/ParserCategories');
const ParserLinks = require('./service/ParserLinks');

logger.info('Script is started');

const validIndustries = [
  'Sales',
  'Marketing',
  'Analytics',
  'Content Management',
  'ERP',
  'Commerce',
  'Customer Service',
];

const main = async () => {
  await IO.Init();
  const allCategories = await ParserCategories.GetAll();
  const desiredCategories = allCategories.filter(category =>
    validIndustries.includes(category.industryRootName)
  );

  // eslint-disable-next-line no-restricted-syntax
  for (const category of desiredCategories) {
    // eslint-disable-next-line no-await-in-loop
    await ParserLinks.ParseGategory(category);
  }
};

main().then(() => {
  logger.info('Script is finished.');
  return IO.EndResult();
});
